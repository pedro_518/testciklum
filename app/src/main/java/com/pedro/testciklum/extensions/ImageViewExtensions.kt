package com.pedro.testciklum.extensions

import android.widget.ImageView
import androidx.annotation.ColorInt
import androidx.databinding.BindingAdapter

@BindingAdapter("app:tint")
fun ImageView.setTint(@ColorInt color: Int) {
    this.drawable.setTint(color)
}