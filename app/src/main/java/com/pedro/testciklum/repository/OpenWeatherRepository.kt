package com.pedro.testciklum.repository

import arrow.core.Either
import com.pedro.testciklum.model.UnitsFormat
import com.pedro.testciklum.model.WeatherInfo

interface OpenWeatherRepository {

    suspend fun findWeather(cityOrZipCode : String, units: UnitsFormat = UnitsFormat.Metric) : Either<Throwable, WeatherInfo>
    suspend fun findWeatherByName(city : String, units: UnitsFormat = UnitsFormat.Metric) : Either<Throwable, WeatherInfo>
    suspend fun findWeatherByZip(city : String, units: UnitsFormat = UnitsFormat.Metric) : Either<Throwable, WeatherInfo>
    suspend fun findWeatherByCoord(lat: Double, lon: Double, units: UnitsFormat = UnitsFormat.Metric) : Either<Throwable, WeatherInfo>

}