package com.pedro.testciklum.repository

import arrow.core.Either
import com.pedro.testciklum.model.UnitsFormat
import com.pedro.testciklum.model.WeatherInfo

interface OpenWeatherRemoteRepository {

    suspend fun findWeatherByName(city : String, units: UnitsFormat = UnitsFormat.Metric) : Either<Throwable, WeatherInfo>
    suspend fun findWeatherByZip(zipCode : String, units: UnitsFormat = UnitsFormat.Metric) : Either<Throwable, WeatherInfo>
    suspend fun findWeatherByCoord(lat: Double, lon: Double, units: UnitsFormat = UnitsFormat.Metric) : Either<Throwable, WeatherInfo>

}