package com.pedro.testciklum.repository.impl

import arrow.core.Either
import com.pedro.testciklum.model.UnitsFormat
import com.pedro.testciklum.model.WeatherInfo
import com.pedro.testciklum.repository.OpenWeatherRemoteRepository
import com.pedro.testciklum.repository.OpenWeatherRepository
import com.pedro.testciklum.util.ZipCodeValidator

class OpenWeatherRepositoryImpl(
    private val openWeatherRemoteRepository : OpenWeatherRemoteRepository
) : OpenWeatherRepository {
    override suspend fun findWeatherByCoord(lat: Double, lon: Double, units: UnitsFormat): Either<Throwable, WeatherInfo> =
        openWeatherRemoteRepository.findWeatherByCoord(lat, lon)

    override suspend fun findWeather(cityOrZipCode : String, units: UnitsFormat) : Either<Throwable, WeatherInfo> {
        return if(ZipCodeValidator.isZipCode(cityOrZipCode)) {
            findWeatherByZip(cityOrZipCode, units)
        } else {
            findWeatherByName(cityOrZipCode, units)
        }
    }
    override suspend fun findWeatherByName(city : String, units: UnitsFormat) : Either<Throwable, WeatherInfo> =
        openWeatherRemoteRepository.findWeatherByName(city, units)

    override suspend fun findWeatherByZip(city : String, units: UnitsFormat) : Either<Throwable, WeatherInfo> =
        openWeatherRemoteRepository.findWeatherByZip(city, units)

}