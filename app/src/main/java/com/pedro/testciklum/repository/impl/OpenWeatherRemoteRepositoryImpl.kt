package com.pedro.testciklum.repository.impl

import arrow.core.Either
import arrow.core.Try
import com.pedro.testciklum.api.OpenWeatherApi
import com.pedro.testciklum.model.UnitsFormat
import com.pedro.testciklum.model.WeatherInfo
import com.pedro.testciklum.repository.OpenWeatherRemoteRepository

class OpenWeatherRemoteRepositoryImpl(
    private val apiKey: String,
    private val openWeatherApi: OpenWeatherApi
) : OpenWeatherRemoteRepository {
    override suspend fun findWeatherByCoord(lat: Double, lon: Double, units: UnitsFormat): Either<Throwable, WeatherInfo> =
        Try {
            openWeatherApi.findByCoord(lat, lon, units.name, apiKey)
        }.toEither()

    override suspend fun findWeatherByName(city : String, units: UnitsFormat) : Either<Throwable, WeatherInfo> =
        Try {
            openWeatherApi.findByName(city, units.name, apiKey)
        }.toEither()

    override suspend fun findWeatherByZip(zipCode : String, units: UnitsFormat) : Either<Throwable, WeatherInfo> =
        Try {
            openWeatherApi.findByZip(zipCode, units.name, apiKey)
        }.toEither()

}