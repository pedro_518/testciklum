package com.pedro.testciklum.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.pedro.testciklum.databinding.WeatherStaticsAdapterBinding
import com.pedro.testciklum.model.WeatherInfoStatics

class WeatherStaticsAdapter : ListAdapter<WeatherInfoStatics, WeatherStaticsAdapter.WeatherStaticViewHolder>(diffCallback) {

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<WeatherInfoStatics>(){
            override fun areItemsTheSame(oldItem: WeatherInfoStatics, newItem: WeatherInfoStatics): Boolean =
                oldItem.weatherInfo.id == newItem.weatherInfo.id

            override fun areContentsTheSame(oldItem: WeatherInfoStatics, newItem: WeatherInfoStatics): Boolean =
                oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherStaticViewHolder {
        val binding = WeatherStaticsAdapterBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return WeatherStaticViewHolder(binding)
    }

    override fun onBindViewHolder(holder: WeatherStaticViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class WeatherStaticViewHolder(val binding: WeatherStaticsAdapterBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: WeatherInfoStatics) {
            binding.item = item
            binding.executePendingBindings()
        }

    }
}