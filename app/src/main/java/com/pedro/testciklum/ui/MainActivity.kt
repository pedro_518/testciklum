package com.pedro.testciklum.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.pedro.testciklum.R
import com.pedro.testciklum.databinding.ActivityMainBinding
import com.pedro.testciklum.ui.adapter.WeatherStaticsAdapter
import com.pedro.testciklum.ui.viewmodel.MainViewModel
import org.koin.android.ext.android.bind
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val mainViewModel: MainViewModel by viewModel()

    private lateinit var binding: ActivityMainBinding

    private val adapter by lazy { WeatherStaticsAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(binding.toolbar)
        binding.lifecycleOwner = this
        binding.mainViewModel = mainViewModel

        mainViewModel.liveData.observe(this, Observer {
            binding.toolbar.title = it.first().weatherInfo.name
            adapter.submitList(it)
        })

        mainViewModel.findWeather("Málaga")

        binding.rvStatics.adapter = adapter

        initListeners()
    }

    private fun initListeners() {
        binding.bFind.setOnClickListener {
            find(binding.etFind.text.toString())
        }
    }

    private fun find(cityOrZip: String) {
        mainViewModel.findWeather(cityOrZip)
    }
}