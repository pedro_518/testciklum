package com.pedro.testciklum.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import arrow.core.orNull
import com.pedro.testciklum.model.WeatherInfoStatics
import com.pedro.testciklum.repository.OpenWeatherRepository
import com.pedro.testciklum.util.CoordUtil
import com.pedro.testciklum.util.WeatherTracker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(
    private val openWeatherRepository: OpenWeatherRepository
) : ViewModel() {

    private val dist = 200000.0

    val liveData: MutableLiveData<List<WeatherInfoStatics>> = MutableLiveData()
    val loading: MutableLiveData<Boolean> = MutableLiveData()
    val error: MutableLiveData<Error?> = MutableLiveData()

    fun findWeather(cityOrZipCode: String) = viewModelScope.launch(Dispatchers.IO) {
        withContext(Dispatchers.Main) {
            loading.value = true
            error.value = null
        }
        val originWeather = openWeatherRepository.findWeather(cityOrZipCode.trim())

        originWeather.fold(
                ifLeft = {
                    withContext(Dispatchers.Main) {
                        error.value = Error(cityOrZipCode)
                        loading.value = false
                    }
                },
                ifRight = {
                    val eastFind = async {
                        val eastCoord = CoordUtil.getCoords(it.coord, 0.0, dist)
                        openWeatherRepository.findWeatherByCoord(eastCoord.lat, eastCoord.lon)
                    }

                    val nothFind = async {
                        val nothCoord = CoordUtil.getCoords(it.coord, 90.0, dist)
                        openWeatherRepository.findWeatherByCoord(nothCoord.lat, nothCoord.lon)
                    }

                    val wesstFind = async {
                        val westCoord = CoordUtil.getCoords(it.coord, 180.0, dist)
                        openWeatherRepository.findWeatherByCoord(westCoord.lat, westCoord.lon)
                    }

                    val southFind = async {
                        val southCoord = CoordUtil.getCoords(it.coord, 270.0, dist)
                        openWeatherRepository.findWeatherByCoord(southCoord.lat, southCoord.lon)
                    }

                    val result = listOf(
                            it,
                            nothFind.await().orNull(),
                            southFind.await().orNull(),
                            eastFind.await().orNull(),
                            wesstFind.await().orNull()
                    )

                    val statics = result
                            .filterNotNull()
                            .map {weatherInfo ->
                                WeatherInfoStatics(
                                        weatherInfo = weatherInfo,
                                        isHighestTemperature = WeatherTracker.getHighestTemperature(result) == weatherInfo,
                                        isHighestHumidity = WeatherTracker.getHighestHumidity(result) == weatherInfo,
                                        isMoreWindy = WeatherTracker.getMoreWindy(result) == weatherInfo,
                                        isRainiest = WeatherTracker.getRainest(result) == weatherInfo
                                )
                            }

                    withContext(Dispatchers.Main) {
                        liveData.value = statics
                        loading.value = false
                        error.value = null
                    }
                }
        )

    }

    data class Error(val search: String)
}