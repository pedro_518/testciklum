package com.pedro.testciklum.di

import com.google.gson.Gson
import com.pedro.testciklum.BuildConfig
import com.pedro.testciklum.api.ApiUtils
import com.pedro.testciklum.api.OpenWeatherApi
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val apiModule = module {

    single {
        Retrofit.Builder()
            .baseUrl(BuildConfig.HOST)
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build()
    }

    single { ApiUtils.createApi<OpenWeatherApi>(get()) }
}