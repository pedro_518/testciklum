package com.pedro.testciklum.di

import com.pedro.testciklum.BuildConfig
import com.pedro.testciklum.repository.OpenWeatherRemoteRepository
import com.pedro.testciklum.repository.impl.OpenWeatherRemoteRepositoryImpl
import org.koin.dsl.module

val remoteRepositoryModule = module {
    single<OpenWeatherRemoteRepository> {
        OpenWeatherRemoteRepositoryImpl(
            apiKey = BuildConfig.API_KEY,
            openWeatherApi = get()
        )
    }
}