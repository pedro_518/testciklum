package com.pedro.testciklum.di

import com.pedro.testciklum.repository.OpenWeatherRepository
import com.pedro.testciklum.repository.impl.OpenWeatherRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {

    single<OpenWeatherRepository> {
        OpenWeatherRepositoryImpl(
            openWeatherRemoteRepository = get()
        )
    }
}