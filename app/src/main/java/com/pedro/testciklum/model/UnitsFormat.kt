package com.pedro.testciklum.model

sealed class UnitsFormat {

    abstract val name: String

    object Metric : UnitsFormat() {
        override val name: String = "metric"
    }

}