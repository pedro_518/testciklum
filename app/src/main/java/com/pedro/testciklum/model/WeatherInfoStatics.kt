package com.pedro.testciklum.model

data class WeatherInfoStatics(
        val weatherInfo: WeatherInfo,
        val isHighestTemperature: Boolean,
        val isHighestHumidity: Boolean,
        val isRainiest: Boolean,
        val isMoreWindy: Boolean
)