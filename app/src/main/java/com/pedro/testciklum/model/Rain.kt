package com.pedro.testciklum.model

import com.google.gson.annotations.SerializedName

data class Rain(
        @SerializedName("1h")
        val lastHour: Double?
)