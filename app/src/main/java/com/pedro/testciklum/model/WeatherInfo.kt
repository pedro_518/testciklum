package com.pedro.testciklum.model

import com.google.gson.annotations.SerializedName

data class WeatherInfo(

    @SerializedName("id")
    val id : Long,

    @SerializedName("coord")
    val coord : Coord,

    @SerializedName("weather")
    val weather : List<Weather>,

    @SerializedName("base")
    val base : String,

    @SerializedName("main")
    val main : WeatherMain,

    @SerializedName("visibility")
    val visibility : Int,

    @SerializedName("rain")
    val rain : Rain?,

    @SerializedName("wind")
    val wind : Wind,

    @SerializedName("clouds")
    val clouds : Clouds,

    @SerializedName("dt")
    val dt : Long,

    @SerializedName("name")
    val name : String?
)