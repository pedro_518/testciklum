package com.pedro.testciklum.api

import com.pedro.testciklum.model.WeatherInfo
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherApi {

    @GET("weather")
    suspend fun findByZip(
            @Query("zip") zipCode: String,
            @Query("units") units: String,
            @Query("APPID") apiKey: String
    ) : WeatherInfo

    @GET("weather")
    suspend fun findByName(
            @Query("q") city: String,
            @Query("units") units: String,
            @Query("APPID")apiKey: String
    ) : WeatherInfo

    @GET("weather")
    suspend fun findByCoord(
            @Query("lat") lat: Double,
            @Query("lon") lon: Double,
            @Query("units") units: String,
            @Query("APPID")apiKey: String) : WeatherInfo

}