package com.pedro.testciklum.util

import com.pedro.testciklum.model.WeatherInfo

object WeatherTracker {

    fun getHighestTemperature(weatherInfos: List<WeatherInfo?>) : WeatherInfo? {
        return weatherInfos.filterNotNull().maxBy { it.main.temp }
    }

    fun getHighestHumidity(weatherInfos: List<WeatherInfo?>) : WeatherInfo? {
        return weatherInfos.filterNotNull().maxBy { it.main.humidity }
    }

    fun getRainest(weatherInfos: List<WeatherInfo?>) : WeatherInfo? {
        return weatherInfos.filterNotNull().maxBy { it.rain?.lastHour ?: 0.0 }
    }

    fun getMoreWindy(weatherInfos: List<WeatherInfo?>) : WeatherInfo? {
        return weatherInfos.filterNotNull().maxBy { it.wind.speed }
    }

}