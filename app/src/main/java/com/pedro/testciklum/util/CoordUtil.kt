package com.pedro.testciklum.util

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil
import com.pedro.testciklum.model.Coord


object CoordUtil {

    fun getCoords(origin: Coord, degrees: Double, dist: Double) : Coord {

        val res = SphericalUtil.computeOffset(LatLng(origin.lat, origin.lon), dist, degrees)

        return Coord(
            lon = res.longitude,
            lat = res.latitude
        )

    }

}