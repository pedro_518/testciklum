package com.pedro.testciklum.util

object ZipCodeValidator {

    private val ZIP_CODE_PATTERN = Regex("^[0-9]{5}(?:-[0-9]{4})?\$")

    fun isZipCode(zipCode: String) : Boolean =
        zipCode.matches(ZIP_CODE_PATTERN)

}