package com.pedro.testciklum

import android.app.Application
import com.pedro.testciklum.di.*
import org.koin.core.context.startKoin

class CiklumApp: Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            modules(
                listOf(
                    appModule,
                    apiModule,
                    remoteRepositoryModule,
                    repositoryModule,
                    viewModelModule
                )
            )
        }
    }
}