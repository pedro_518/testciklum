package com.pedro.testciklum

import com.pedro.testciklum.util.ZipCodeValidator
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class ZipCodeValidatorTest {

    @Test
    fun checkZipCode_valid() {
        val zipCode = "18014"
        assertTrue(ZipCodeValidator.isZipCode(zipCode))
    }

    @Test
    fun checkZipCode_no_valid() {
        val zipCode = "18.14"
        assertFalse(ZipCodeValidator.isZipCode(zipCode))
    }

    @Test
    fun checkZipCode_no_valid_letters() {
        val zipCode = "asfadv"
        assertFalse(ZipCodeValidator.isZipCode(zipCode))
    }

}