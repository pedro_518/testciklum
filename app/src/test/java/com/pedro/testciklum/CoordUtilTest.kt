package com.pedro.testciklum

import com.pedro.testciklum.model.Coord
import com.pedro.testciklum.util.CoordUtil
import junit.framework.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class CoordUtilTest {

    private val granada = Coord(-3.6, 37.18)

    @Test
    fun getCoordsTest() {
        val coord = CoordUtil.getCoords(granada, 0.0, 200000.0)
        assertEquals(-3.597929, coord.lon, 0.1)
        assertEquals(38.9746235, coord.lat, 0.1)
    }
}
